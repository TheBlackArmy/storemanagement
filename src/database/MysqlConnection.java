package database;

import java.sql.*;

public abstract class MysqlConnection {
    String connectionUrl;
    String host;
    String user;
    String password;
    String dbname;
    int port;
    Connection cursor;


    public MysqlConnection(String host, String user, String password, String dbname, int port) {
        this.host = host;
        this.user = user;
        this.password = password;
        this.dbname = dbname;
        this.port = port;
        this.makeConnection();
    }

    public MysqlConnection(String host, String user, String password, String dbname) {
        this.host = host;
        this.user = user;
        this.password = password;
        this.dbname = dbname;
        this.port = 3306;
        this.makeConnection();
    }

    private void makeConnection() {
        this.connectionUrl = "jdbc:mysql://" + this.host + ":" + this.port + "/" + this.dbname;
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            this.cursor = DriverManager.getConnection(this.connectionUrl, this.user, this.password);
        } catch (Exception e) {
            System.err.println(e);
        }
    }

    public Connection getCursor() {
        return cursor;
    }
}
