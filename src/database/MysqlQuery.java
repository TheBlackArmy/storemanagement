package database;

import views.layouts.CustomDialog;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

public class MysqlQuery extends MysqlConnection {
    Statement statement;
    String lastQueryString;
    ResultSet lastResultSet;

    public MysqlQuery(String host, String user, String password, String dbname, int port) {
        super(host, user, password, dbname, port);
        this.makeStatement();
    }

    public MysqlQuery(String host, String user, String password, String dbname) {
        super(host, user, password, dbname);
        this.makeStatement();
    }

    public boolean query(String query) {
        try {
            this.lastResultSet = this.statement.executeQuery(query);
            lastQueryString = query;
            return true;
        } catch (java.sql.SQLException e) {
            try {
                this.statement.execute(query);
                lastQueryString = query;
                return true;
            } catch (java.sql.SQLIntegrityConstraintViolationException constraintViolation) {
                System.err.println(constraintViolation.getMessage());
                new CustomDialog("Constraint Error", constraintViolation.getMessage());
            } catch (Exception e2) {
                System.err.println("Error(from "+ this.getClass().getName() +"): "+ e2);
            }
//            System.err.println("Error(from "+ this.getClass().getName() +"): "+ e);
        }

        return false;
    }

    public ResultSet getLastResultSet() {
        return lastResultSet;
    }

    public String getLastQueryString() {
        return lastQueryString;
    }

    private void makeStatement() {
        try {
            this.statement = this.getCursor().createStatement();
        } catch (Exception e) {
            System.err.println(e);
        }
    }
}
