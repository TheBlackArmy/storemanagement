package views.provision;

import controllers.ProductsController;
import controllers.ProvisionsController;
import models.Product;
import models.Provision;
import providers.ProductComboBoxModel;
import views.AppLayout;
import views.layouts.CustomDialog;

import javax.swing.*;
import java.awt.event.*;
import java.sql.SQLException;

public class UpdateProvision extends JDialog {
    private final ProductComboBoxModel productComboBoxModel;
    private JButton buttonOK;
    private JButton buttonCancel;
    private JLabel productLabel;
    private JComboBox productLabelField;
    private JLabel quantityLabel;
    private JSpinner quantityField;
    private JLabel noteLable;
    private JTextArea noteField;
    private JPanel contentPane;
    private Provision provision;

    public UpdateProvision(Provision provision) {
        this.provision = provision;
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);
        setTitle("Update provision");
        fill();

        buttonOK.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                try {
                    onOK();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
        });

        buttonCancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        });

        // call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

        // call onCancel() on ESCAPE
        contentPane.registerKeyboardAction(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);

        quantityField.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent e) {
                super.keyReleased(e);
                try {
                    Integer.parseInt(quantityField.getValue().toString());
                } catch (Exception excep) {
                    quantityField.setValue(0);
                }
            }
        });

        productComboBoxModel = new ProductComboBoxModel(ProductsController.getAll());
        productLabelField.setModel(productComboBoxModel);

        pack();
        setVisible(true);
    }

    private void onOK() throws SQLException {
        if(productComboBoxModel != null) {
            Product product = productComboBoxModel.getSelectedProduct();

            if(product != null && !quantityField.getValue().equals(0) ) {
                Provision provision = ProvisionsController.update(this.provision.getId(), product.getId(), (Integer) quantityField.getValue(), noteField.getText());

                if (provision != null) {
                    dispose();
                    AppLayout.getjSplitPane().setRightComponent(new ShowProvison(provision).getContainerPane());
                    new CustomDialog("Success", "Provision Updated Successfully");
                } else {
                    new CustomDialog("Error", "Error Saving new provision");
                }
            }
        }
    }

    private void onCancel() {
        // add your code here if necessary
        dispose();
    }

    public void fill() {
        Product product = ProductsController.show(provision.getProduct_id());
        productLabelField.setSelectedItem(product.getLabel());
        quantityField.setValue(provision.getQuantity());
        noteField.setText(provision.getNote());
    }
}
