package views.provision;

import controllers.ProvisionsController;
import models.Provision;
import models.Sale;
import providers.ProductsTableModel;
import providers.ProvisionsTableModel;
import providers.SalesTableModel;
import providers.TableButtonRenderer;
import views.AppLayout;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.ArrayList;

public class ListProvision {
    private JPanel provisionsListPanel;
    private JButton createNewProvisionButton;
    private JScrollPane tableScrollPane;
    private JTable provisionsListTable;
    private JTextField searchField;
    private TableButtonRenderer tableButtonRenderer;

    public ListProvision() throws NoSuchMethodException {
        createNewProvisionButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                CreateProvisionDialog.main();
            }
        });

        ArrayList<Provision> provisions = ProvisionsController.getAll();
        ProvisionsTableModel provisionsTableModel = new ProvisionsTableModel(provisions);
        provisionsListTable.setModel(provisionsTableModel);

        tableButtonRenderer = new TableButtonRenderer(provisions, this.getClass(), Provision.class);
        provisionsListTable.getColumnModel().getColumn(4).setCellRenderer(tableButtonRenderer);

        searchField.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent e) {
                super.keyReleased(e);
                String searchKeyword = searchField.getText();
                ArrayList<Provision> provisions1 = new Provision().customSearch(searchKeyword, "label");
                provisionsListTable.setModel( new ProvisionsTableModel(provisions1));
                try {
                    tableButtonRenderer = new TableButtonRenderer(provisions1, ListProvision.class, Provision.class);
                } catch (NoSuchMethodException ex) {
                    ex.printStackTrace();
                }
                provisionsListTable.getColumnModel().getColumn(4).setCellRenderer(tableButtonRenderer);
                provisionsListTable.updateUI();
            }
        });
    }

    public JPanel getProvisionsListPanel() {
        return provisionsListPanel;
    }

    public static void callback(Provision provision) {
        AppLayout.getjSplitPane().setRightComponent(new ShowProvison(provision).getContainerPane());
    }
}
