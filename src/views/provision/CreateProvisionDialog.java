package views.provision;

import controllers.ProductsController;
import controllers.ProvisionsController;
import controllers.WarehouseProductsController;
import models.Product;
import models.Provision;
import models.Warehouse;
import providers.ProductComboBoxModel;
import views.AppLayout;
import views.layouts.CustomDialog;

import javax.swing.*;
import java.awt.event.*;

public class CreateProvisionDialog extends JDialog {
    private final ProductComboBoxModel productComboBoxModel;
    private JPanel contentPane;
    private JButton buttonOK;
    private JButton buttonCancel;
    private JLabel productLabel;
    private JComboBox productLabelField;
    private JLabel quantityLabel;
    private JSpinner quantityField;
    private JLabel noteLable;
    private JTextArea noteField;

    public CreateProvisionDialog() {
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);
        setTitle("Create new provision");

        buttonOK.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                try {
                    onOK();
                } catch (NoSuchMethodException ex) {
                    ex.printStackTrace();
                }
            }
        });

        buttonCancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        });

        // call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

        // call onCancel() on ESCAPE
        contentPane.registerKeyboardAction(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);

        productComboBoxModel = new ProductComboBoxModel(ProductsController.getAll());
        productLabelField.setModel(productComboBoxModel);

        quantityField.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent e) {
                super.keyReleased(e);
                try {
                    Integer.parseInt(quantityField.getValue().toString());
                } catch (Exception excep) {
                    quantityField.setValue(0);
                }
            }
        });
    }

    private void onOK() throws NoSuchMethodException {
        if(productComboBoxModel != null) {
            Product product = productComboBoxModel.getSelectedProduct();

            if(product != null && !quantityField.getValue().equals(0) ) {
                Provision provision = ProvisionsController.create(product.getId(), (Integer) quantityField.getValue(), noteField.getText());

                if (provision != null) {
                    dispose();
                    AppLayout.getjSplitPane().setRightComponent(new ListProvision().getProvisionsListPanel());
                    new CustomDialog("Success", "New Provision Created Successfully");
                } else {
                    new CustomDialog("Create Error", "Error Saving new provision");
                }
            }
        }
    }

    private void onCancel() {
        // add your code here if necessary
        dispose();
    }

    public static void main() {
        CreateProvisionDialog dialog = new CreateProvisionDialog();
        dialog.pack();
        dialog.setVisible(true);
    }
}
