package views.provision;

import controllers.ProductsController;
import models.Product;
import models.Provision;
import views.AppLayout;
import views.layouts.CustomDialog;
import views.user.ListUser;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ShowProvison {
    private JButton returnButton;
    private JButton updateButton;
    private JButton softDeleteButton;
    private JPanel containerPane;
    private JLabel productLabel;
    private JLabel quantity;
    private JLabel note;
    private JLabel createdAtLabel;
    private JLabel updatedAtLabel;
    private JLabel deletedAtLabel;
    private JLabel provisionFor;
    private Provision provision;
    private Product product;

    public ShowProvison(Provision provision) {
        this.provision = provision;
        product = ProductsController.show(provision.getProduct_id());
        provisionFor.setText("Provision for "+ product.getLabel());
        fill();

        returnButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                try {
                    AppLayout.getjSplitPane().setRightComponent(new ListProvision().getProvisionsListPanel());
                } catch (NoSuchMethodException e) {
                    e.printStackTrace();
                }
            }
        });

        updateButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                new UpdateProvision(provision);
            }
        });

        softDeleteButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                if(new Provision().delete(provision.getId())) {
                    new CustomDialog("Success", "Provision deleted");
                    try {
                        AppLayout.getjSplitPane().setRightComponent(new ListProvision().getProvisionsListPanel());
                    } catch (NoSuchMethodException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }

    public void fill() {
        productLabel.setText(product.getLabel());
        quantity.setText(Integer.toString(provision.getQuantity()));
        note.setText(provision.getNote());
        createdAtLabel.setText(product.getCreated_at());
        updatedAtLabel.setText(product.getUpdated_at());
        deletedAtLabel.setText(product.getDeleted_at());
    }

    public JPanel getContainerPane() {
        return containerPane;
    }
}
