package views.sale;

import controllers.SalesController;
import models.Product;
import models.Sale;
import providers.ProductsTableModel;
import providers.SalesTableModel;
import providers.TableButtonRenderer;
import views.AppLayout;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.ArrayList;

public class ListSale {
    private JPanel salesListPanel;
    private JButton createNewSaleButton;
    private JScrollPane tableScrollPane;
    private JTable salesListTable;
    private JTextField searchField;
    private TableButtonRenderer tableButtonRenderer;

    public ListSale() throws NoSuchMethodException {
        createNewSaleButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                CreateSaleDialog.main();
            }
        });


        ArrayList<Sale> sales = SalesController.getAll();
        SalesTableModel salesTableModel = new SalesTableModel(sales);

        salesListTable.setModel(salesTableModel);

        tableButtonRenderer = new TableButtonRenderer(sales, this.getClass(), Sale.class);
        salesListTable.getColumnModel().getColumn(7).setCellRenderer(tableButtonRenderer);

        searchField.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent e) {
                super.keyReleased(e);
                String searchKeyword = searchField.getText();
                ArrayList<Sale> sales1 = new Sale().customSearch(searchKeyword, "label");
                salesListTable.setModel( new SalesTableModel(sales1));
                try {
                    tableButtonRenderer = new TableButtonRenderer(sales1, ListSale.class, Sale.class);
                } catch (NoSuchMethodException ex) {
                    ex.printStackTrace();
                }
                salesListTable.getColumnModel().getColumn(7).setCellRenderer(tableButtonRenderer);
                salesListTable.updateUI();
            }
        });
    }

    public JPanel getSalesListPanel() {
        return salesListPanel;
    }

    public static void callback(Sale sale) {
        AppLayout.getjSplitPane().setRightComponent(new ShowSale(sale).getContainerPane());
    }
}
