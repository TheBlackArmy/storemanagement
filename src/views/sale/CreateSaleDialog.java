package views.sale;

import controllers.ProductsController;
import controllers.SalesController;
import models.Product;
import models.Sale;
import org.jetbrains.annotations.NotNull;
import providers.ProductComboBoxModel;
import views.AppLayout;
import views.layouts.CustomDialog;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.sql.SQLException;
import java.util.ArrayList;

public class CreateSaleDialog extends JDialog {
    private JPanel contentPane;
    private JButton buttonOK;
    private JButton buttonCancel;
    private JComboBox productLabelField;
    private JSpinner quantityField;
    private JLabel productLabel;
    private JLabel quantityLabel;
    private ProductComboBoxModel productComboBoxModel;

    public CreateSaleDialog() {
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);
        setTitle("Create new sale");

        buttonOK.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                try {
                    onOK();
                } catch (SQLException | NoSuchMethodException ex) {
                    ex.printStackTrace();
                }
            }
        });

        buttonCancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        });

        // call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

        // call onCancel() on ESCAPE
        contentPane.registerKeyboardAction(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);

        quantityField.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent e) {
                super.keyReleased(e);
                try {
                    Integer.parseInt(quantityField.getValue().toString());
                } catch (Exception excep) {
                    quantityField.setValue(0);
                }
            }
        });

        productComboBoxModel = new ProductComboBoxModel(ProductsController.getAll());
        productLabelField.setModel(productComboBoxModel);
    }

    private void onOK() throws SQLException, NoSuchMethodException {
        if(productComboBoxModel != null) {
            Product product = productComboBoxModel.getSelectedProduct();

            if(product != null && !quantityField.getValue().equals(0) ) {
                Sale sale = SalesController.create(product.getId(), (Integer) quantityField.getValue());
                if (sale != null) {
                    dispose();
                    AppLayout.getjSplitPane().setRightComponent(new ListSale().getSalesListPanel());
                    new CustomDialog("Success", "New Sale Created Successfully");
                } else {
                    new CustomDialog("Create Error", "Error Saving new Sale");
                }
            }
        }
    }

    private void onCancel() {
        // add your code here if necessary
        dispose();
    }

    public static void main() {
        CreateSaleDialog dialog = new CreateSaleDialog();
        dialog.pack();
        dialog.setVisible(true);
    }
}
