package views.sale;

import controllers.ProductsController;
import controllers.SalesController;
import controllers.UsersController;
import models.Product;
import models.Sale;
import models.User;
import providers.ProductComboBoxModel;
import views.AppLayout;
import views.layouts.CustomDialog;

import javax.swing.*;
import java.awt.event.*;

public class UpdateSale extends JDialog {
    private final ProductComboBoxModel productComboBoxModel;
    private JButton buttonOK;
    private JButton buttonCancel;
    private JLabel productLabel;
    private JComboBox productLabelField;
    private JLabel quantityLabel;
    private JSpinner quantityField;
    private JPanel contentPane;
    private  Sale sale;

    public UpdateSale(Sale sale) {
        this.sale = sale;

        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);
        setTitle("Update sale");
        fill();

        buttonOK.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onOK();
            }
        });

        buttonCancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        });

        // call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

        // call onCancel() on ESCAPE
        contentPane.registerKeyboardAction(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);

        quantityField.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent e) {
                super.keyReleased(e);
                try {
                    Integer.parseInt(quantityField.getValue().toString());
                } catch (Exception excep) {
                    quantityField.setValue(0);
                }
            }
        });

        productComboBoxModel = new ProductComboBoxModel(ProductsController.getAll());
        productLabelField.setModel(productComboBoxModel);

        pack();
        setVisible(true);
    }

    private void onOK() {
        if(productComboBoxModel != null) {
            Product product = productComboBoxModel.getSelectedProduct();

            if(product != null && !quantityField.getValue().equals(0) ) {
                Sale sale = SalesController.update(this.sale.getId(), product.getId(), (Integer) quantityField.getValue());

                if (sale != null) {
                    dispose();
                    AppLayout.getjSplitPane().setRightComponent(new ShowSale(sale).getContainerPane());
                    new CustomDialog("Success", "Sale Updated Successfully");
                } else {
                    new CustomDialog("Error", "Error Saving Sale");
                }
            }
        }
    }

    private void onCancel() {
        // add your code here if necessary
        dispose();
    }

    public void fill() {
        Product product = ProductsController.show(sale.getProduct_id());
        User user = UsersController.show(sale.getUser_id());

        productLabelField.setSelectedItem(product.getLabel());
        quantityField.setValue(sale.getQuantity());
    }
}
