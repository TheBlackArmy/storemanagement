package views.sale;

import controllers.ProductsController;
import controllers.ProvisionsController;
import models.Product;
import models.Provision;
import models.Sale;
import views.AppLayout;
import views.layouts.CustomDialog;
import views.provision.ListProvision;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ShowSale {
    private JButton returnButton;
    private JButton updateButton;
    private JButton softDeleteButton;
    private JPanel containerPane;
    private JLabel productLabel;
    private JLabel quantity;
    private JLabel salePrice;
    private JLabel totalPrice;
    private JLabel tva;
    private JLabel createdAtLabel;
    private JLabel updatedAtLabel;
    private JLabel deletedAtLabel;
    private JLabel saleOf;
    private Sale sale;
    Product product;

    public ShowSale(Sale sale) {
        this.sale = sale;
        product = ProductsController.show(sale.getProduct_id());
        saleOf.setText("Sale of "+ product.getLabel());
        fill();

        returnButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                try {
                    AppLayout.getjSplitPane().setRightComponent(new ListSale().getSalesListPanel());
                } catch (NoSuchMethodException e) {
                    e.printStackTrace();
                }
            }
        });

        updateButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                new UpdateSale(sale);
            }
        });

        softDeleteButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                if(new Sale().delete(sale.getId())) {
                    new CustomDialog("Success", "Sale deleted");
                    try {
                        AppLayout.getjSplitPane().setRightComponent(new ListSale().getSalesListPanel());
                    } catch (NoSuchMethodException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }

    public void fill() {
        productLabel.setText(product.getLabel());
        quantity.setText(Integer.toString(sale.getQuantity()));
        salePrice.setText(Double.toString(product.getSale_price()));
        totalPrice.setText( Double.toString(sale.getQuantity() * product.getSale_price()) );
        tva.setText(Double.toString(product.getTva()));
        createdAtLabel.setText(product.getCreated_at());
        updatedAtLabel.setText(product.getUpdated_at());
        deletedAtLabel.setText(product.getDeleted_at());
    }

    public JPanel getContainerPane() {
        return containerPane;
    }
}
