package views.warehouse;

import controllers.WarehouseProductsController;
import models.Sale;
import models.Warehouse;
import providers.SalesTableModel;
import providers.TableButtonRenderer;
import providers.WarehouseTableModel;
import views.AppLayout;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.ArrayList;

public class ListWarehouseProduct {
    private JPanel warehouseProductListPanel;
    private JButton createNewStockProductButton;
    private JScrollPane tableScrollPane;
    private JTable warehouseListTable;
    private JTextField searchField;
    private TableButtonRenderer tableButtonRenderer;

    public ListWarehouseProduct() throws NoSuchMethodException {
        createNewStockProductButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                CreateWarehouseProductDialog.main();
            }
        });

        ArrayList<Warehouse> warehouses = WarehouseProductsController.getAll();
        WarehouseTableModel warehouseTableModel = new WarehouseTableModel(warehouses);
        warehouseListTable.setModel(warehouseTableModel);

        tableButtonRenderer = new TableButtonRenderer(warehouses, this.getClass(), Warehouse.class);
        warehouseListTable.getColumnModel().getColumn(4).setCellRenderer(tableButtonRenderer);

        searchField.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent e) {
                super.keyReleased(e);
                String searchKeyword = searchField.getText();
                ArrayList<Warehouse> warehouses1 = new Warehouse().customSearch(searchKeyword, "label");
                warehouseListTable.setModel( new WarehouseTableModel(warehouses1));
                try {
                    tableButtonRenderer = new TableButtonRenderer(warehouses1, ListWarehouseProduct.class, Warehouse.class);
                } catch (NoSuchMethodException ex) {
                    ex.printStackTrace();
                }
                warehouseListTable.getColumnModel().getColumn(4).setCellRenderer(tableButtonRenderer);
                warehouseListTable.updateUI();
            }
        });
    }

    public JPanel getWarehouseProductListPanel() {
        return warehouseProductListPanel;
    }

    public static void callback(Warehouse warehouse) {
        AppLayout.getjSplitPane().setRightComponent(new ShowWarehouseProduct(warehouse).getContainerPane());
    }
}
