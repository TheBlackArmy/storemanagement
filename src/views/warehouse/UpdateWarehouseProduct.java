package views.warehouse;

import controllers.ProductsController;
import controllers.ProvisionsController;
import controllers.WarehouseProductsController;
import models.Product;
import models.Provision;
import models.Warehouse;
import providers.ProductComboBoxModel;
import views.AppLayout;
import views.layouts.CustomDialog;

import javax.swing.*;
import java.awt.event.*;

public class UpdateWarehouseProduct extends JDialog {
    private final ProductComboBoxModel productComboBoxModel;
    private JButton buttonOK;
    private JButton buttonCancel;
    private JLabel productLabel;
    private JComboBox productLabelField;
    private JLabel quantityLabel;
    private JSpinner quantityField;
    private JLabel noteLabel;
    private JTextArea noteField;
    private JPanel contentPane;
    private Warehouse warehouse;

    public UpdateWarehouseProduct(Warehouse warehouse) {
        this.warehouse = warehouse;
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);
        setTitle("Update warehouse");
        fill();

        buttonOK.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onOK();
            }
        });

        buttonCancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        });

        // call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

        // call onCancel() on ESCAPE
        contentPane.registerKeyboardAction(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);

        quantityField.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent e) {
                super.keyReleased(e);
                try {
                    Integer.parseInt(quantityField.getValue().toString());
                } catch (Exception excep) {
                    quantityField.setValue(0);
                }
            }
        });

        productComboBoxModel = new ProductComboBoxModel(ProductsController.getAll());
        productLabelField.setModel(productComboBoxModel);

        pack();
        setVisible(true);
    }

    private void onOK() {
        if(productComboBoxModel != null) {
            Product product = productComboBoxModel.getSelectedProduct();

            if(product != null && !quantityField.getValue().equals(0) ) {
                Warehouse warehouse = WarehouseProductsController.update(this.warehouse.getId(), product.getId(), (Integer) quantityField.getValue(), noteField.getText());

                if (warehouse != null) {
                    dispose();
                    AppLayout.getjSplitPane().setRightComponent(new ShowWarehouseProduct(warehouse).getContainerPane());
                    new CustomDialog("Success", "Warehouse Updated Successfully");
                } else {
                    new CustomDialog("Error", "Error Saving new warehouse");
                }
            }
        }
    }

    private void onCancel() {
        // add your code here if necessary
        dispose();
    }

    public void fill() {
        Product product = ProductsController.show(warehouse.getProduct_id());
        productLabelField.setSelectedItem(product.getLabel());
        quantityField.setValue(warehouse.getQuantity());
        noteField.setText(warehouse.getNote());
    }
}
