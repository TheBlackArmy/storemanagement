package views.warehouse;

import controllers.ProductsController;
import models.Product;
import models.Provision;
import models.Warehouse;
import views.AppLayout;
import views.layouts.CustomDialog;
import views.provision.ListProvision;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ShowWarehouseProduct {
    private JButton returnButton;
    private JButton updateButton;
    private JButton softDeleteButton;
    private JPanel containerPane;
    private JLabel productLabel;
    private JLabel quantity;
    private JLabel note;
    private JLabel createdAtLabel;
    private JLabel updatedAtLabel;
    private JLabel deletedAtLabel;
    private JLabel productConcerned;
    private Warehouse warehouse;
    private Product product;


    public ShowWarehouseProduct(Warehouse warehouse) {
        this.warehouse = warehouse;
        product = ProductsController.show(warehouse.getProduct_id());
        productConcerned.setText("In stock for "+ product.getLabel());
        fill();

        returnButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                try {
                    AppLayout.getjSplitPane().setRightComponent(new ListWarehouseProduct().getWarehouseProductListPanel());
                } catch (NoSuchMethodException e) {
                    e.printStackTrace();
                }
            }
        });

        updateButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                new UpdateWarehouseProduct(warehouse);
            }
        });

        softDeleteButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                if(new Warehouse().delete(warehouse.getId())) {
                    new CustomDialog("Success", "Warehouse deleted");
                    try {
                        AppLayout.getjSplitPane().setRightComponent(new ListProvision().getProvisionsListPanel());
                    } catch (NoSuchMethodException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }

    public void fill() {
        productLabel.setText(product.getLabel());
        quantity.setText(Integer.toString(warehouse.getQuantity()));
        note.setText(warehouse.getNote());
        createdAtLabel.setText(product.getCreated_at());
        updatedAtLabel.setText(product.getUpdated_at());
        deletedAtLabel.setText(product.getDeleted_at());
    }

    public JPanel getContainerPane() {
        return containerPane;
    }
}
