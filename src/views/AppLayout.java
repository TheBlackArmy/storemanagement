package views;

import views.layouts.HomePage;
import views.layouts.LeftPanelMenu;

import javax.swing.*;
import java.awt.*;
import java.sql.SQLException;

public class AppLayout extends JFrame {
    private static JSplitPane jSplitPane;
    private static JFrame instance = null;

    public AppLayout() throws HeadlessException, SQLException {
        this.setMinimumSize(new Dimension(1150, 700));

        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        if(this.instance != null) {
            // pass
        } else {

            // prolog
            this.setTitle("Store Management");
            this.setPreferredSize(new Dimension(1150, 700));
            this.setLocation(100, 150);


            this.jSplitPane = new JSplitPane();
            this.jSplitPane.setOrientation(JSplitPane.HORIZONTAL_SPLIT);
            this.jSplitPane.setLeftComponent( (new LeftPanelMenu()).getLeftPanel() );
            this.jSplitPane.setRightComponent( (new HomePage()).getHomepagePanel() );
            this.jSplitPane.setDividerSize(0);

            this.add(this.jSplitPane);

            this.pack();

            instance = this;
        }

    }

    public JFrame getInstance() {
        return instance;
    }

    public static JSplitPane getjSplitPane() {
        return jSplitPane;
    }
}
