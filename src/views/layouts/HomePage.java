package views.layouts;

import controllers.GlobalInstances;
import database.MysqlQuery;
import models.Product;
import models.Provision;
import models.Sale;
import models.User;

import javax.swing.*;
import java.sql.SQLException;

public class HomePage {
    private JPanel homepagePanel;
    private JLabel salesSum;
    private JLabel productsSum;
    private JLabel outOfStockSum;
    private JLabel provisionsSum;
    private JLabel earningSum;
    private JLabel usersSum;

    public HomePage() throws SQLException {
        salesSum.setText( Integer.toString(new Sale().getCount()) +" sales");
        productsSum.setText( Integer.toString(new Product().getCount()) + " products");
        provisionsSum.setText( Integer.toString(new Provision().getCount()) +" times");
        usersSum.setText( Integer.toString(new User().getCount()) + " users");

        MysqlQuery mysqlQuery = GlobalInstances.getCursor();
        mysqlQuery.query("SELECT COUNT(*) AS count FROM warehouses WHERE quantity < 10 AND deleted_at IS NULL");
        if(mysqlQuery.getLastResultSet().next()) {
            outOfStockSum.setText(Integer.toString( mysqlQuery.getLastResultSet().getInt(1) ) + " products");
        } else {
            outOfStockSum.setText("0 products");
        }

        mysqlQuery.query("SELECT SUM(sales.quantity*products.sale_price) AS salesSum FROM sales JOIN products ON sales.product_id = products.id");
        if(mysqlQuery.getLastResultSet().next()) {
            earningSum.setText(Integer.toString( mysqlQuery.getLastResultSet().getInt(1) )+ " XOF");
        } else {
            earningSum.setText("0 XOF");
        }
    }

    public JPanel getHomepagePanel() {
        return homepagePanel;
    }
}
