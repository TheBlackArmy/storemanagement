package views.layouts;

import controllers.GlobalInstances;
import views.AppLayout;
import views.product.ListProduct;
import views.provision.ListProvision;
import views.sale.ListSale;
import views.user.ListUser;
import models.User;
import views.warehouse.ListWarehouseProduct;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;

public class LeftPanelMenu {
    private JPanel leftPanel;
    private JLabel menuLabel;
    private JPanel connectedUserPanel;
    private JLabel connectedUserLabel;
    private JLabel connectedUsernameLabel;
    private JButton productsButton;
    private JButton salesButton;
    private JButton warehouseButton;
    private JButton usersButton;
    private JButton provisionsButton;
    private JButton logOutButton;
    private JButton home;
    private JTree menu;

    public LeftPanelMenu() {

        User connectedUser = GlobalInstances.getConnectedUser();

        connectedUsernameLabel.setText( connectedUser.getFirstname()+ " " + connectedUser.getLastname());

        productsButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                try {
                    AppLayout.getjSplitPane().setRightComponent( (new ListProduct()).getProductsListPanel() );
                } catch (NoSuchMethodException e) {
                    e.printStackTrace();
                }
            }
        });

        salesButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                try {
                    AppLayout.getjSplitPane().setRightComponent( (new ListSale()).getSalesListPanel() );
                } catch (NoSuchMethodException e) {
                    e.printStackTrace();
                }
            }
        });

        provisionsButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                try {
                    AppLayout.getjSplitPane().setRightComponent( (new ListProvision()).getProvisionsListPanel() );
                } catch (NoSuchMethodException e) {
                    e.printStackTrace();
                }
            }
        });

        warehouseButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                try {
                    AppLayout.getjSplitPane().setRightComponent( (new ListWarehouseProduct()).getWarehouseProductListPanel() );
                } catch (NoSuchMethodException e) {
                    e.printStackTrace();
                }
            }
        });

        usersButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                try {
                    AppLayout.getjSplitPane().setRightComponent( new ListUser().getUsersListPanel() );
                } catch (NoSuchMethodException e) {
                    e.printStackTrace();
                }
            }
        });

        logOutButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                int userRes = JOptionPane.showConfirmDialog(null, "Would you like to log out ?");
                if(userRes == 0) {
                    AppLayout appLayout = null;
                    try {
                        appLayout = new AppLayout();
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                    appLayout.getInstance().dispose();
                }
            }
        });

        home.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                try {
                    AppLayout.getjSplitPane().setRightComponent( (new HomePage()).getHomepagePanel() );
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public JPanel getLeftPanel() {
        return leftPanel;
    }

    public boolean isModified(AppLayout data) {
        return false;
    }

    public void setData(LeftPanelMenu data) {
        this.connectedUsernameLabel.setText(data.connectedUsernameLabel.getText());
    }

    public void getData(LeftPanelMenu data) {
    }

    public boolean isModified(LeftPanelMenu data) {
        return false;
    }

}
