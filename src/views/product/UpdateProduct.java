package views.product;

import com.sun.jdi.JDIPermission;
import controllers.ProductsController;
import models.Product;
import views.AppLayout;
import views.layouts.CustomDialog;
import views.user.ShowUser;

import javax.swing.*;
import javax.swing.plaf.BorderUIResource;
import java.awt.event.*;

public class UpdateProduct extends JDialog {
    private JButton buttonOK;
    private JButton buttonCancel;
    private JLabel productLabel;
    private JTextField productLabelField;
    private JLabel descriptionLabel;
    private JLabel buyPriceLabel;
    private JTextField buyPriceField;
    private JLabel sellPriceLabel;
    private JTextField sellPriceField;
    private JLabel tvaLabel;
    private JTextField tvaField;
    private JTextArea descriptionField;
    private JPanel contentPane;
    private Product product;

    public UpdateProduct(Product product) {
        descriptionField.setBorder(new BorderUIResource.EtchedBorderUIResource());
        this.product = product;
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);
        setTitle("Create new product");
        fill();

        descriptionField.setBorder(new BorderUIResource.EtchedBorderUIResource());

        buttonOK.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onOK();
            }
        });

        buttonCancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        });

        // call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

        // call onCancel() on ESCAPE
        contentPane.registerKeyboardAction(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);

        buyPriceField.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent e) {
                super.keyReleased(e);
                try {
                    Double.parseDouble(buyPriceField.getText());
                } catch (Exception excep) {
                    buyPriceField.setText("");
                }
            }
        });

        sellPriceField.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent e) {
                super.keyReleased(e);
                try {
                    Double.parseDouble(sellPriceField.getText());
                } catch (Exception excep) {
                    sellPriceField.setText("");
                }
            }
        });

        tvaField.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent e) {
                super.keyReleased(e);
                try {
                    Double.parseDouble(tvaField.getText());
                } catch (Exception excep) {
                    tvaField.setText("");
                }
            }
        });

        pack();
        setVisible(true);
    }


    private void onOK() {
        // add your code here
        String label = this.productLabelField.getText().replaceAll("'", "\\\\'");
        String description = this.descriptionField.getText().replaceAll("'", "\\\\'");

        Double buy_price = this.buyPriceField.getText().isEmpty() ? null : Double.parseDouble(this.buyPriceField.getText());
        Double sale_price = this.sellPriceField.getText().isEmpty() ? null : Double.parseDouble(this.sellPriceField.getText());
        Double tva = this.tvaField.getText().isEmpty() ? null : Double.parseDouble(this.tvaField.getText());

        if(!label.isEmpty() && buy_price != null && sale_price != null && tva != null ) {
            Product product = ProductsController.update(this.product.getId(), label, description, buy_price, sale_price, tva);
            if (product != null) {
                dispose();
                AppLayout.getjSplitPane().setRightComponent(new ShowProduct(product).getContainerPane());
                new CustomDialog("Success", "Product updated Successfully");
            } else {
                new CustomDialog("Error", "Error updating Product");
            }
        }

    }

    public void fill() {
        productLabelField.setText(product.getLabel());
        descriptionField.setText(product.getDescription());
        buyPriceField.setText(Double.toString(product.getBuy_price()));
        sellPriceField.setText(Double.toString(product.getSale_price()));
        tvaField.setText(Double.toString(product.getTva()));
    }

    private void onCancel() {
        // add your code here if necessary
        dispose();
    }
}
