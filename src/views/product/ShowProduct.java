package views.product;

import models.Product;
import views.AppLayout;
import views.layouts.CustomDialog;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ShowProduct {
    private JButton returnButton;
    private JButton updateButton;
    private JButton softDeleteButton;
    private JPanel containerPane;
    private JLabel productLabel;
    private JLabel productDesciption;
    private JLabel buyPrice;
    private JLabel salePrice;
    private JLabel createdAtLabel;
    private JLabel updatedAtLabel;
    private JLabel deletedAtLabel;
    private JLabel productName;
    private JLabel tva;
    private Product product;

    public ShowProduct(Product product) {
        this.product = product;
        productName.setText(product.getLabel());
        fill();

        returnButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                try {
                    AppLayout.getjSplitPane().setRightComponent(new ListProduct().getProductsListPanel());
                } catch (NoSuchMethodException e) {
                    e.printStackTrace();
                }
            }
        });

        updateButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                new UpdateProduct(product);
            }
        });

        softDeleteButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                if(new Product().delete(product.getId())) {
                    new CustomDialog("Success", "Product deleted");
                    try {
                        AppLayout.getjSplitPane().setRightComponent(new ListProduct().getProductsListPanel());
                    } catch (NoSuchMethodException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }

    public void fill() {
        productLabel.setText(product.getLabel());
        productDesciption.setText(product.getDescription());
        buyPrice.setText(Double.toString(product.getBuy_price()));
        salePrice.setText(Double.toString(product.getSale_price()));
        tva.setText(Double.toString(product.getTva()));
        createdAtLabel.setText(product.getCreated_at());
        updatedAtLabel.setText(product.getUpdated_at());
        deletedAtLabel.setText(product.getDeleted_at());
    }

    public JPanel getContainerPane() {
        return containerPane;
    }
}
