package views.product;

import controllers.ProductsController;
import models.Product;
import providers.ProductsTableModel;
import providers.TableButtonRenderer;
import views.AppLayout;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.ArrayList;

public class ListProduct {
    private JPanel productListPanel;
    private JButton createNewProductButton;
    private JScrollPane tableScrollPane;
    private JTable productsListTable;
    private JTextField searchField;
    private TableButtonRenderer tableButtonRenderer;

    public ListProduct() throws NoSuchMethodException {
        createNewProductButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                CreateProductDialog.main();
            }
        });

        ArrayList<Product> products = ProductsController.getAll();

        productsListTable.setModel(new ProductsTableModel(products));

        tableButtonRenderer = new TableButtonRenderer(products, this.getClass(), Product.class);
        productsListTable.getColumnModel().getColumn(5).setCellRenderer(tableButtonRenderer);

        searchField.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent e) {
                super.keyReleased(e);
                String searchKeyword = searchField.getText();
                ArrayList<Product> products1 = new Product().search(searchKeyword, "label");
                productsListTable.setModel( new ProductsTableModel(products1));
                try {
                    tableButtonRenderer = new TableButtonRenderer(products1, ListProduct.class, Product.class);
                } catch (NoSuchMethodException ex) {
                    ex.printStackTrace();
                }
                productsListTable.getColumnModel().getColumn(5).setCellRenderer(tableButtonRenderer);
                productsListTable.updateUI();
            }
        });

    }

    public void setData(ListProduct data) {
    }

    public void getData(ListProduct data) {
    }

    public boolean isModified(ListProduct data) {
        return false;
    }

    public JPanel getProductsListPanel() {
        return productListPanel;
    }

    public  static void callback(Product product) {
        AppLayout.getjSplitPane().setRightComponent(new ShowProduct(product).getContainerPane());
    }
}
