package views.product;

import controllers.ProductsController;
import models.Product;
import views.AppLayout;
import views.layouts.CustomDialog;

import javax.swing.*;
import javax.swing.plaf.BorderUIResource;
import java.awt.*;
import java.awt.event.*;

public class CreateProductDialog extends JDialog {
    private JPanel contentPane;
    private JButton buttonOK;
    private JButton buttonCancel;
    private JLabel productLabel;
    private JTextField productLabelField;
    private JLabel descriptionLabel;
    private JLabel buyPriceLabel;
    private JTextField buyPriceField;
    private JLabel sellPriceLabel;
    private JTextField sellPriceField;
    private JLabel tvaLabel;
    private JTextField tvaField;
    private JTextArea descriptionField;

    public CreateProductDialog() {
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);
        setTitle("Create new product");

        descriptionField.setBorder(new BorderUIResource.EtchedBorderUIResource());

        buttonOK.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                try {
                    onOK();
                } catch (NoSuchMethodException ex) {
                    ex.printStackTrace();
                }
            }
        });

        buttonCancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        });

        // call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

        // call onCancel() on ESCAPE
        contentPane.registerKeyboardAction(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);

        buyPriceField.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent e) {
                super.keyReleased(e);
                try {
                    Double.parseDouble(buyPriceField.getText());
                } catch (Exception excep) {
                    buyPriceField.setText("");
                }
            }
        });

        sellPriceField.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent e) {
                super.keyReleased(e);
                try {
                    Double.parseDouble(sellPriceField.getText());
                } catch (Exception excep) {
                    sellPriceField.setText("");
                }
            }
        });

        tvaField.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent e) {
                super.keyReleased(e);
                try {
                    Double.parseDouble(tvaField.getText());
                } catch (Exception excep) {
                    tvaField.setText("");
                }
            }
        });
    }


    private void onOK() throws NoSuchMethodException {
        // add your code here
        String label = this.productLabelField.getText().replaceAll("'", "\\\\'");
        String description = this.descriptionField.getText().replaceAll("'", "\\\\'");

        Double buy_price = this.buyPriceField.getText().isEmpty() ? null : Double.parseDouble(this.buyPriceField.getText());
        Double sale_price = this.sellPriceField.getText().isEmpty() ? null : Double.parseDouble(this.sellPriceField.getText());
        Double tva = this.tvaField.getText().isEmpty() ? null : Double.parseDouble(this.tvaField.getText());

        if(!label.isEmpty() && buy_price != null && sale_price != null && tva != null ) {
            Product product = ProductsController.create(label, description, buy_price, sale_price, tva);
            if (product != null) {
                dispose();
                AppLayout.getjSplitPane().setRightComponent(new ListProduct().getProductsListPanel());
                new CustomDialog("Success", "New Product Created Successfully");
            } else {
                new CustomDialog("Create Error", "Error Creating new Product");
            }
        }

    }

    private void onCancel() {
        // add your code here if necessary
        dispose();
    }

    public static void main() {
        CreateProductDialog dialog = new CreateProductDialog();
        dialog.pack();
        dialog.setVisible(true);
    }
}
