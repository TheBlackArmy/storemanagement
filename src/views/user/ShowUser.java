package views.user;

import controllers.GlobalInstances;
import models.User;
import views.AppLayout;
import views.layouts.CustomDialog;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ShowUser {
    private JButton returnButton;
    private JButton updateButton;
    private JButton softDeleteButton;
    private JPanel containerPane;
    private JLabel firstnameLabel;
    private JLabel lastnameLabel;
    private JLabel loginLabel;
    private JLabel phoneLabel;
    private JLabel emailLabel;
    private JLabel createdAtLabel;
    private JLabel updatedAtLabel;
    private JLabel deletedAtLabel;
    private JLabel username;
    private User user;

    public ShowUser(User user) {
        this.user = user;
        this.fill();
        username.setText(user.getFirstname()+" "+user.getLastname());

        if(user.getLogin().equals(GlobalInstances.getConnectedUser().getLogin())) {
            softDeleteButton.setVisible(false);
        }


        returnButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                try {
                    AppLayout.getjSplitPane().setRightComponent(new ListUser().getUsersListPanel());
                } catch (NoSuchMethodException e) {
                    e.printStackTrace();
                }
            }
        });

        updateButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                new UpdateUser(user);
            }
        });

        softDeleteButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                if(new User().delete(user.getId())) {
                    new CustomDialog("Success", "User deleted");
                    try {
                        AppLayout.getjSplitPane().setRightComponent(new ListUser().getUsersListPanel());
                    } catch (NoSuchMethodException e) {
                        e.printStackTrace();
                    }
                }

            }
        });
    }

    private void fill() {
        firstnameLabel.setText(user.getFirstname());
        lastnameLabel.setText(user.getLastname());
        loginLabel.setText(user.getLogin());
        phoneLabel.setText(user.getPhone());
        emailLabel.setText(user.getEmail());
        createdAtLabel.setText(user.getCreated_at());
        updatedAtLabel.setText(user.getUpdated_at());
        deletedAtLabel.setText(user.getDeleted_at());
    }

    public JPanel getContainerPane() {
        return containerPane;
    }
}
