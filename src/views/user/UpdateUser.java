package views.user;

import controllers.UsersController;
import models.User;
import views.AppLayout;
import views.layouts.CustomDialog;

import javax.swing.*;
import java.awt.event.*;

public class UpdateUser extends JDialog {
    private JButton buttonOK;
    private JButton buttonCancel;
    private JLabel firstnameLabel;
    private JTextField firstnameField;
    private JLabel lastnameLabel;
    private JTextField lastnameField;
    private JLabel loginLabel;
    private JTextField loginField;
    private JLabel phoneLabel;
    private JTextField phoneField;
    private JLabel emailLabel;
    private JTextField emailField;
    private JLabel passwordLabel;
    private JPasswordField passwordField;
    private JPanel contentPane;
    private User user;

    public UpdateUser(User user) {
        this.user = user;
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);
        setTitle("Update user");
        fill();

        buttonOK.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onOK();
            }
        });

        buttonCancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        });

        // call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

        // call onCancel() on ESCAPE
        contentPane.registerKeyboardAction(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);

        pack();
        setVisible(true);
    }

    private void onOK() {
        String firstname = firstnameField.getText();
        String lastname = lastnameField.getText();
        String login = loginField.getText();
        String phone = phoneField.getText();
        String email = emailField.getText();
        String password = passwordField.getText();

        if(!firstname.isEmpty() && !lastname.isEmpty() && !login.isEmpty() && !phone.isEmpty() && !email.isEmpty() && !password.isEmpty()) {
            if (UsersController.update(user.getId(), firstname, lastname, login, phone, email, password) == null) {
                new CustomDialog("Error", "An error occurs when updating the user");
            } else {
                dispose();
                AppLayout.getjSplitPane().setRightComponent(new ShowUser(user).getContainerPane());
                new CustomDialog("Success", "User updated: \nLogin: " + login + "\nPassword: " + password);
            }
        }
    }

    private void onCancel() {
        dispose();
    }

    private void fill() {
        firstnameField.setText(user.getFirstname());
        lastnameField.setText(user.getLastname());
        loginField.setText(user.getLogin());
        phoneField.setText(user.getPhone());
        emailField.setText(user.getEmail());
    }

    public void setData(UpdateUser data) {
    }

    public void getData(UpdateUser data) {
    }

    public boolean isModified(UpdateUser data) {
        return false;
    }
}
