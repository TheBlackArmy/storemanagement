package views.user;

import controllers.UsersController;
import models.Sale;
import models.User;
import providers.SalesTableModel;
import providers.TableButtonRenderer;
import providers.UsersTableModel;
import views.AppLayout;
import views.sale.ShowSale;

import javax.swing.*;
import java.awt.event.*;
import java.util.ArrayList;

public class ListUser {
    private JPanel usersListPanel;
    private JButton createNewUserButton;
    private JTable usersListTable;
    private JScrollPane tableScrollPane;
    private JTextField searchField;
    private TableButtonRenderer tableButtonRenderer;

    public ListUser() throws NoSuchMethodException {
        createNewUserButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                CreateUserDialog.main();
            }
        });


        ArrayList<User> users = UsersController.getAll();

        usersListTable.setModel(new UsersTableModel(users));

        tableButtonRenderer = new TableButtonRenderer(users, this.getClass(), User.class);
        usersListTable.getColumnModel().getColumn(5).setCellRenderer(tableButtonRenderer);

        searchField.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent e) {
                super.keyReleased(e);
                String searchKeyword = searchField.getText();
                ArrayList<User> users1 = new User().search(searchKeyword, "firstname");
                usersListTable.setModel( new UsersTableModel(users1));
                try {
                    tableButtonRenderer = new TableButtonRenderer(users1, ListUser.class, User.class);
                } catch (NoSuchMethodException ex) {
                    ex.printStackTrace();
                }
                usersListTable.getColumnModel().getColumn(5).setCellRenderer(tableButtonRenderer);
                usersListTable.updateUI();
            }
        });

    }

    public JPanel getUsersListPanel() {
        return usersListPanel;
    }

    public static void callback(User user) {
        AppLayout.getjSplitPane().setRightComponent(new ShowUser(user).getContainerPane());
    }
}
