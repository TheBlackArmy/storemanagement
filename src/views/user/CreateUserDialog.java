package views.user;

import controllers.UsersController;
import views.AppLayout;
import views.layouts.CustomDialog;

import javax.swing.*;
import java.awt.event.*;

public class CreateUserDialog extends JDialog {
    private JPanel contentPane;
    private JButton buttonOK;
    private JButton buttonCancel;
    private JLabel firstnameLabel;
    private JTextField firstnameField;
    private JLabel lastnameLabel;
    private JTextField lastnameField;
    private JLabel loginLabel;
    private JTextField loginField;
    private JLabel phoneLabel;
    private JTextField phoneField;
    private JLabel emailLabel;
    private JTextField emailField;
    private JLabel passwordLabel;
    private JPasswordField passwordField;

    public CreateUserDialog() {
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);
        setTitle("Create new user");

        buttonOK.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                try {
                    onOK();
                } catch (NoSuchMethodException ex) {
                    ex.printStackTrace();
                }
            }
        });

        buttonCancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        });

        // call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

        // call onCancel() on ESCAPE
        contentPane.registerKeyboardAction(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
    }

    private void onOK() throws NoSuchMethodException {
        String firstname = firstnameField.getText();
        String lastname = lastnameField.getText();
        String login = loginField.getText();
        String phone = phoneField.getText();
        String email = emailField.getText();
        String password = passwordField.getText();

        if(!firstname.isEmpty() && !lastname.isEmpty() && !login.isEmpty() && !phone.isEmpty() && !email.isEmpty() && !password.isEmpty()) {
            if(UsersController.create(firstname, lastname, login, phone, email, password) == null) {
                new CustomDialog("Error", "An error occurs when creating the user");
            } else {
                dispose();
                AppLayout.getjSplitPane().setRightComponent(new ListUser().getUsersListPanel());
                new CustomDialog("Success", "New user created: \nLogin: "+login+ "\nPassword: "+password);
            }
        }
    }

    private void onCancel() {
        // add your code here if necessary
        dispose();
    }

    public static void main() {
        CreateUserDialog dialog = new CreateUserDialog();
        dialog.pack();
        dialog.setVisible(true);
    }
}
