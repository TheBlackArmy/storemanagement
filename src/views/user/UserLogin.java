package views.user;

import controllers.GlobalInstances;
import database.MysqlQuery;
import models.User;
import views.AppLayout;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.ArrayList;

public class UserLogin {
    private JTextField emailField;
    private JPasswordField passwordField;
    private JPanel contentPane;
    private JLabel loginLabel;
    private JLabel passwordLabel;
    private JButton loginButton;
    private JLabel errorLabel;
    private JFrame loginWindow;

    public UserLogin() {

        ArrayList<JTextField> textFields = new ArrayList<JTextField>();
        textFields.add(emailField);
        textFields.add(passwordField);

        ArrayList<JButton> buttons = new ArrayList<JButton>();
        buttons.add(loginButton);

        for(JTextField textField : textFields) {
            textField.setBorder(BorderFactory.createEmptyBorder());
        }

        for(JButton button : buttons) {
            button.setBorder(BorderFactory.createEmptyBorder());
        }

        loginWindow = new JFrame("User Login");
        loginWindow.setContentPane(contentPane);
        loginWindow.setResizable(false);
        loginWindow.setLocation(400, 100);
        loginWindow.pack();
        loginWindow.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        loginButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                String login = emailField.getText();
                String password = passwordField.getText();

                MysqlQuery mysqlQuery = GlobalInstances.getCursor();
                mysqlQuery.query("SELECT * FROM users WHERE login = '"+ login +"' AND password = '"+ password +"' AND deleted_at IS NULL LIMIT 1");
                try {
                    User connectedUser = new User();
                    if(mysqlQuery.getLastResultSet().next()) {

                        connectedUser.setId(mysqlQuery.getLastResultSet().getInt(1));
                        connectedUser.setFirstname(mysqlQuery.getLastResultSet().getString(2));
                        connectedUser.setLastname(mysqlQuery.getLastResultSet().getString(3));
                        connectedUser.setLogin(mysqlQuery.getLastResultSet().getString(4));
                        connectedUser.setPhone(mysqlQuery.getLastResultSet().getString(5));
                        connectedUser.setEmail(mysqlQuery.getLastResultSet().getString(6));
                        connectedUser.setPassword(mysqlQuery.getLastResultSet().getString(7));

                        GlobalInstances.setConnectedUser(connectedUser);
                        AppLayout appLayout = new AppLayout();
                        appLayout.getInstance().setVisible(true);
                        loginWindow.dispose();
                    } else {
                        errorLabel.setText("Login or password not in our records");
                        errorLabel.setVisible(true);
                    }

                } catch (Exception e) {
                    System.err.println(e);
                    errorLabel.setText(e.toString());
                    errorLabel.setVisible(true);
                }
            }
        });

    }

    public void display() {
        loginWindow.setVisible(true);
    }
}
