package providers;

import models.Product;
import models.Provision;
import models.Sale;
import models.User;

import javax.swing.table.AbstractTableModel;
import java.util.ArrayList;

public class ProvisionsTableModel extends BaseTableModel {

    private ArrayList<Provision> provisions;

    public ProvisionsTableModel(ArrayList<Provision> provisions) {
        super(provisions);
        this.provisions = getObjects();
        this.setColumnNames(new String[] { "PRODUCT", "QUANTITY", "NOTE", "CREATED AT","ACTION" });
    }

    @Override
    public Object getValueAt(int row, int col) {

        Product product = new Product().find(provisions.get(row).getProduct_id());

        Object object = null;
        switch (col) {
            case 0:
                object = product.getLabel();
                break;
            case 1:
                object = provisions.get(row).getQuantity();
                break;
            case 2:
                object = provisions.get(row).getNote();
                break;
            case 3:
                object = provisions.get(row).getCreated_at();
                break;
            case 4:
                object = "show";
                break;
        }
        return object;
    }
}
