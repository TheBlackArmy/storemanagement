package providers;

import models.Product;

import javax.swing.table.AbstractTableModel;
import java.util.ArrayList;

public abstract class BaseTableModel extends AbstractTableModel {
    private ArrayList<?> objects;
    private String[] columnNames;

    public BaseTableModel( ArrayList<?> objects) {
        this.objects = objects;
    }

    public <T> ArrayList<T> getObjects() {
        return (ArrayList<T>) objects;
    }

    public void setColumnNames(String[] columnNames) {
        this.columnNames = columnNames;
    }

    @Override
    public int getRowCount() {
        return this.objects.size();
    }

    @Override
    public int getColumnCount() {
        return this.columnNames.length;
    }

    @Override
    public abstract Object getValueAt(int row, int col);

    public String getColumnName(int col) {
        return columnNames[col];
    }
}
