package providers;

import models.Product;
import models.Provision;
import models.Warehouse;

import javax.swing.table.AbstractTableModel;
import java.util.ArrayList;

public class WarehouseTableModel extends BaseTableModel {

    private ArrayList<Warehouse> warehouses;

    public WarehouseTableModel(ArrayList<Warehouse> warehouses) {
        super(warehouses);
        this.warehouses = getObjects();
        this.setColumnNames(new String[] { "PRODUCT", "QUANTITY", "NOTE", "CREATED AT","ACTION" });
    }

    @Override
    public Object getValueAt(int row, int col) {

        Product product = new Product().find(warehouses.get(row).getProduct_id());

        Object object = null;
        switch (col) {
            case 0:
                object = product.getLabel();
                break;
            case 1:
                object = warehouses.get(row).getQuantity();
                break;
            case 2:
                object = warehouses.get(row).getNote();
                break;
            case 3:
                object = warehouses.get(row).getCreated_at();
                break;
            case 4:
                object = "show";
                break;
        }
        return object;
    }
}
