package providers;

import javax.swing.*;
import javax.swing.table.TableCellRenderer;
import java.awt.*;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;

public class TableButtonRenderer extends JButton implements TableCellRenderer {
    private  ArrayList<?> arrayList;
    private Class callbackObjectClass;
    private Method  callback;

    public TableButtonRenderer(ArrayList<?> arrayList, Class<?> objectClass, Class<?> parameterType) throws NoSuchMethodException {
        this.arrayList = arrayList;
        this.callbackObjectClass = objectClass;

        this.callback = this.callbackObjectClass.getMethod("callback", parameterType);

    }


    @Override
    public Component getTableCellRendererComponent(JTable jTable, Object object, boolean isSelected, boolean isFocused, int row, int col) {
        if(object != null)
            setText(object.toString());
        else
            setText("Button");

        if(isSelected) {
            try {
                callback.invoke(callbackObjectClass, arrayList.get(row));
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            }
        }

        return this;
    }
}
