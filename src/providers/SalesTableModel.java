package providers;

import controllers.ProductsController;
import models.Product;
import models.Sale;
import models.User;

import javax.swing.table.AbstractTableModel;
import java.util.ArrayList;

public class SalesTableModel extends BaseTableModel {

    private ArrayList<Sale> sales;

    public SalesTableModel(ArrayList<Sale> sales) {
        super(sales);
        this.sales = getObjects();
        this.setColumnNames(new String[] { "PRODUCT", "QUANTITY", "SALE PRICE", "TOTAL PRICE", "TVA", "USER", "CREATED AT", "ACTION" });
    }

    @Override
    public Object getValueAt(int row, int col) {

        Product product = new Product().find(sales.get(row).getProduct_id());
        User user = new User().find(sales.get(row).getUser_id());

        Object object = null;
        switch (col) {
            case 0:
                object = product.getLabel();
                break;
            case 1:
                object = sales.get(row).getQuantity();
                break;
            case 2:
                object = product.getSale_price();
                break;
            case 3:
                object = product.getSale_price() * sales.get(row).getQuantity();
                break;
            case 4:
                object = product.getTva();
                break;
            case 5:
                object = user.getFirstname()+" "+user.getLastname();
                break;
            case 6:
                object = sales.get(row).getCreated_at();
                break;
            case 7:
                object = "show";
                break;
        }
        return object;
    }
}
