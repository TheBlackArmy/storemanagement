package providers;

import models.Product;
import models.Sale;

import javax.swing.*;
import javax.swing.event.ListDataListener;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;

public class ProductComboBoxModel implements ComboBoxModel {

    private ArrayList<Product> products;
    private Object selectedObject;
    private Product selectedProduct;

    public ProductComboBoxModel(ArrayList<Product> products) {
        this.products = products;
    }

    public Product getSelectedProduct() {
        return selectedProduct;
    }

    @Override
    public void setSelectedItem(Object o) {
        this.selectedObject = o;
        for (Product product : products) {
            if (product.getLabel().equals(this.selectedObject)) {
                this.selectedProduct = product;
                break;
            }
        }
    }

    @Override
    public Object getSelectedItem() {
        return this.selectedObject;
    }

    @Override
    public int getSize() {
        return this.products.size();
    }

    @Override
    public Object getElementAt(int i) {
        return this.products.get(i).getLabel();
    }

    @Override
    public void addListDataListener(ListDataListener listDataListener) {

    }

    @Override
    public void removeListDataListener(ListDataListener listDataListener) {

    }
}
