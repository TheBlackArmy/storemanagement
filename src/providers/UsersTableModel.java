package providers;

import models.User;

import javax.swing.*;
import javax.swing.event.TableModelListener;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableCellRenderer;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

public class UsersTableModel extends BaseTableModel {
    private ArrayList<User> users;

    public UsersTableModel(ArrayList<User> users) {
        super(users);
        this.users = getObjects();
        setColumnNames(new String[] { "FIRSTNAME", "LASTNAME", "LOGIN", "PHONE", "EMAIL", "ACTION" });
    }

    @Override
    public Object getValueAt(int row, int col) {
        Object object = null;
        switch (col) {
            case 0:
                object = users.get(row).getFirstname();
                break;
            case 1:
                object = users.get(row).getLastname();
                break;
            case 2:
                object = users.get(row).getLogin();
                break;
            case 3:
                object = users.get(row).getPhone();
                break;
            case 4:
                object = users.get(row).getEmail();
                break;
            case 5:
                object = "show";
                break;
        }
        return object;
    }

}

