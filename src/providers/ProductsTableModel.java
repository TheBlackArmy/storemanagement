package providers;

import models.Product;
import java.util.ArrayList;

public class ProductsTableModel extends BaseTableModel {

    private ArrayList<Product> products;

    public ProductsTableModel(ArrayList<Product> products) {
        super(products);
        this.products = getObjects();
        this.setColumnNames(new String[] { "LABEL", "DESCRIPTION", "BUY PRICE", "SALE PRICE", "TVA", "ACTION" });
    }

    @Override
    public Object getValueAt(int row, int col) {
        Object object = null;
        switch (col) {
            case 0:
                object = products.get(row).getLabel();
                break;
            case 1:
                object = products.get(row).getDescription();
                break;
            case 2:
                object = products.get(row).getBuy_price();
                break;
            case 3:
                object = products.get(row).getSale_price();
                break;
            case 4:
                object = products.get(row).getTva();
                break;
            case 5:
                object = "show";
                break;
        }
        return object;
    }

}
