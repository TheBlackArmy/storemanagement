package models;

import controllers.GlobalInstances;
import database.MysqlQuery;

import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class Provision extends BaseModel {
    private int id;
    private int product_id;
    private int quantity;
    private String note;
    private String created_at;
    private String updated_at;
    private String deleted_at;

    public Provision() {
    }

    public Provision(int product_id, int quantity, String note) {
        this.id = -1;
        this.product_id = product_id;
        this.quantity = quantity;
        this.note = note;
    }

    public Provision(int id, int product_id, int quantity, String note) {
        this.id = id;
        this.product_id = product_id;
        this.quantity = quantity;
        this.note = note;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getProduct_id() {
        return product_id;
    }

    public void setProduct_id(int product_id) {
        this.product_id = product_id;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public String getDeleted_at() {
        return deleted_at;
    }

    public void setDeleted_at(String deleted_at) {
        this.deleted_at = deleted_at;
    }

    @Override
    public boolean save() {
        String sql_stmt;
        if(this.id == -1) {
            sql_stmt = "INSERT INTO " + getModel() + " (product_id, quantity, note) VALUES (" +
                    "'" + this.product_id + "', " +
                    "'" + this.quantity + "', " +
                    "'" + this.note + "' " +
                    ")";
        } else {
            // TODO get current timestempt
            sql_stmt = "UPDATE " + getModel() + " SET " +
                    "product_id = '" + this.product_id + "', " +
                    "quantity = '" + this.quantity + "', " +
                    "note = '" + this.note + "', " +
                    "updated_at = '"+ new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date()) +"'" +
                    " WHERE id = " + this.id;
        }

        MysqlQuery mysqlQuery = GlobalInstances.getCursor();
        try {
            if (mysqlQuery.query(sql_stmt)) {
                mysqlQuery.query("SELECT MAX(id) FROM provisions");
                if(mysqlQuery.getLastResultSet().next()) {
                    int id =  mysqlQuery.getLastResultSet().getInt(1);
                    this.setId(id);
                    return true;
                }
            }
        } catch (Exception e) {
            System.err.println("Error: " + e);
        }
        return false;
    }

    @Override
    public Provision fromDatabase(ResultSet resultSet) {
        Provision provision = new Provision();
        try {
            provision.setId(resultSet.getInt(1));
            provision.setProduct_id(resultSet.getInt(2));
            provision.setQuantity(resultSet.getInt(3));
            provision.setNote(resultSet.getString(4));
            provision.setCreated_at(resultSet.getString(5));
            provision.setUpdated_at(resultSet.getString(6));
            provision.setDeleted_at(resultSet.getString(7));
        } catch (Exception e) {
            System.err.println(e);
        }
        return provision;
    }

    public ArrayList<Provision> customSearch(String key, String field) {

        if(key.isEmpty()) {
            return new Provision().all();
        }

        ArrayList<Product> products = new Product().search(key, field);
        ArrayList<Provision> provisions = new ArrayList<Provision>();

        for (Product product : products) {

            if(!new Provision().search(Integer.toString(product.getId()), "product_id").isEmpty()) {
                provisions.addAll( (new Provision().search(Integer.toString(product.getId()), "product_id")) );
            }
        }

        return  provisions;
    }
}
