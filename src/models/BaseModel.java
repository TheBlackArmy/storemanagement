package models;

import controllers.GlobalInstances;
import database.MysqlQuery;
import org.jetbrains.annotations.NotNull;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public abstract class BaseModel {
    protected static String model;

    public BaseModel() {
        model = this.getClass().getSimpleName().toLowerCase();
        if(model.charAt(model.length() -1) != 'a') {
            model += 's';
        }
    }

    public <T> ArrayList<T> all() {
        ArrayList<T> arrayList = new ArrayList<T>();
        MysqlQuery mysqlQuery = GlobalInstances.getCursor();
        mysqlQuery.query("SELECT * FROM " + model + " WHERE deleted_at IS NULL");

        try {
            while (mysqlQuery.getLastResultSet().next()) {
                arrayList.add(this.fromDatabase(mysqlQuery.getLastResultSet()));
            }
        } catch (Exception e) {
            System.err.println(e);
        }

        return arrayList;
    }

    public <T> T find(int id) {
        T obj = null;
        MysqlQuery mysqlQuery = GlobalInstances.getCursor();
        mysqlQuery.query("SELECT * FROM " + model + " WHERE id = " + id + " AND deleted_at IS NULL");
        try {
            if(mysqlQuery.getLastResultSet().next()) {
                obj = this.fromDatabase(mysqlQuery.getLastResultSet());
                return obj;
            }
        } catch (Exception e) {
            System.err.println(e);
        }
        return null;
    }

    @NotNull
    public <T> ArrayList<T> search(String key, String field) {

        MysqlQuery mysqlQuery = GlobalInstances.getCursor();
        ArrayList<T> arrayList = new ArrayList<T>();

        mysqlQuery.query("SELECT * FROM " + model + " WHERE " + field + " LIKE " + "'%" + key +"%'");
        try {
            while (mysqlQuery.getLastResultSet().next()) {
                arrayList.add(this.fromDatabase(mysqlQuery.getLastResultSet()));
            }
        } catch (Exception e) {
            System.err.println(e);
        }

        return (ArrayList<T>) arrayList;
    }

    public abstract boolean save();

    public abstract <T> T fromDatabase(ResultSet resultSet);

    public boolean delete(int id) {
        MysqlQuery mysqlQuery = GlobalInstances.getCursor();
        // TODO get the current datetime
//        String sql = ("UPDATE " + model + " SET deleted_at = '"+ new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date()) +"' WHERE id = " + id);
        mysqlQuery.query("DELETE FROM " + model +" WHERE id = " + id);
        try {
            if (mysqlQuery.getLastResultSet() != null) {
                return true;
            }
        } catch (Exception e) {
            System.err.println("Error(from "+ this.getClass().getName() +"): "+e);
        }
        return false;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        BaseModel.model = model;
    }

    public int getCount() throws SQLException {
        String sql_stmt = "SELECT COUNT(*) AS count FROM "+ model + " WHERE deleted_at IS NULL";
        if(GlobalInstances.getCursor().query(sql_stmt)) {
            if(GlobalInstances.getCursor().getLastResultSet().next()) {
                try {
                    return GlobalInstances.getCursor().getLastResultSet().getInt(1);
                } catch (Exception e) {
                    System.err.println("Error from "+ this.getClass().getName() +": "+ e.getMessage());
                }
            }
        }
        return 0;
    }

    public <T> T where(String field, String[] keysList) throws SQLException {
        String keys = new String();
        keys = "(";
        for (String key : keysList) {
            keys += "'"+key.replaceAll("'", "\\\\'")  +"',";
        }
        char[] characters =  keys.toCharArray();
        characters[characters.length-1] = ')';
        keys = String.copyValueOf(characters);

        String sql_stmt = "SELECT * FROM "+ model + " WHERE " + field + " IN " + keys + "LIMIT 1";

        MysqlQuery mysqlQuery = GlobalInstances.getCursor();
        mysqlQuery.query(sql_stmt);

        if(mysqlQuery.getLastResultSet().next()) {
            return (T) this.fromDatabase(mysqlQuery.getLastResultSet());
        }
        return null;
    }
}
