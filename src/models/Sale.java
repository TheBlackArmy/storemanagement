package models;

import controllers.GlobalInstances;
import database.MysqlQuery;
import org.jetbrains.annotations.NotNull;

import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class Sale extends BaseModel {
    private int id;
    private int product_id;
    private int user_id;
    private int quantity;
    private String created_at;
    private String updated_at;
    private String deleted_at;

    public Sale() {
    }

    public Sale(int product_id, int user_id, int quantity) {
        this.id = -1;
        this.product_id = product_id;
        this.user_id = user_id;
        this.quantity = quantity;
    }

    public Sale(int id, int product_id, int user_id, int quantity) {
        this.id = id;
        this.product_id = product_id;
        this.user_id = user_id;
        this.quantity = quantity;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getProduct_id() {
        return product_id;
    }

    public void setProduct_id(int product_id) {
        this.product_id = product_id;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public String getDeleted_at() {
        return deleted_at;
    }

    public void setDeleted_at(String deleted_at) {
        this.deleted_at = deleted_at;
    }

    @Override
    public boolean save() {
        String sql_stmt;
        if(this.id == -1) {
            sql_stmt = "INSERT INTO " + getModel() + " (product_id, user_id, quantity) VALUES (" +
                    "'" + this.product_id + "', " +
                    "'" + this.user_id + "', " +
                    "'" + this.quantity + "' " +
                    ")";
        } else {
            // TODO get current timestempt
            sql_stmt = "UPDATE " + getModel() + " SET " +
                    "product_id = '" + this.product_id + "', " +
                    "user_id = '" + this.user_id + "', " +
                    "quantity = '" + this.quantity + "', " +
                    "updated_at = '"+ new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date()) +"'" +
                    " WHERE id = " + this.id;
        }

        MysqlQuery mysqlQuery = GlobalInstances.getCursor();
        try {
            if (mysqlQuery.query(sql_stmt)) {
                mysqlQuery.query("SELECT MAX(id) FROM sales");
                if(mysqlQuery.getLastResultSet().next()) {
                    int id =  mysqlQuery.getLastResultSet().getInt(1);
                    this.setId(id);
                    return true;
                }
            }
        } catch (Exception e) {
            System.err.println("Error: " + e);
        }
        return false;
    }

    @Override
    public Sale fromDatabase(ResultSet resultSet) {
        Sale sale = new Sale();
        try {
            sale.setId(resultSet.getInt(1));
            sale.setProduct_id(resultSet.getInt(2));
            sale.setUser_id(resultSet.getInt(3));
            sale.setQuantity(resultSet.getInt(4));
            sale.setCreated_at(resultSet.getString(5));
            sale.setUpdated_at(resultSet.getString(6));
            sale.setDeleted_at(resultSet.getString(7));
        } catch (Exception e) {
            System.err.println(e);
        }
        return sale;
    }

    public ArrayList<Sale> customSearch(String key, String field) {

        if(key.isEmpty()) {
            return new Sale().all();
        }

        ArrayList<Product> products = new Product().search(key, field);
        ArrayList<Sale> sales = new ArrayList<Sale>();

        for (Product product : products) {

            if(!new Sale().search(Integer.toString(product.getId()), "product_id").isEmpty()) {
                sales.addAll( (new Sale().search(Integer.toString(product.getId()), "product_id")) );
            }
        }

        return  sales;
    }
}
