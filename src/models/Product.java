package models;

import controllers.GlobalInstances;
import database.MysqlQuery;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Product extends BaseModel {
    private int id;
    private String label;
    private String description;
    private double buy_price;
    private double sale_price;
    private double tva;
    private String created_at;
    private String updated_at;
    private String deleted_at;

    public Product() {
    }

    public Product(String label, String description, double buy_price, double sale_price, double tva) {
        this.id = -1;
        this.label = label;
        this.description = description;
        this.buy_price = buy_price;
        this.sale_price = sale_price;
        this.tva = tva;
    }

    public Product(int id, String label, String description, double buy_price, double sale_price, double tva) {
        this.id = id;
        this.label = label;
        this.description = description;
        this.buy_price = buy_price;
        this.sale_price = sale_price;
        this.tva = tva;
    }

    public int getId() {
        return id;
    }

    public String getLabel() {
        return label;
    }

    public String getDescription() {
        return description;
    }

    public double getBuy_price() {
        return buy_price;
    }

    public double getSale_price() {
        return sale_price;
    }

    public double getTva() {
        return tva;
    }

    public String getCreated_at() {
        return created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public String getDeleted_at() {
        return deleted_at;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setBuy_price(double buy_price) {
        this.buy_price = buy_price;
    }

    public void setSale_price(double sale_price) {
        this.sale_price = sale_price;
    }

    public void setTva(double tva) {
        this.tva = tva;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public void setDeleted_at(String deleted_at) {
        this.deleted_at = deleted_at;
    }

    @Override
    public boolean save() {
        String sql_stmt;
        if(this.id == -1) {
            sql_stmt = "INSERT INTO " + getModel() + " (label, description, buy_price, sale_price, tva) VALUES (" +
                    "'" + this.label + "', " +
                    "'" + this.description + "', " +
                    "'" + this.buy_price + "', " +
                    "'" + this.sale_price + "', " +
                    "'" + this.tva + "' " +
                    ")";
        } else {
            sql_stmt = "UPDATE " + getModel() + " SET " +
                    "label = '" + this.label + "', " +
                    "description = '" + this.description + "', " +
                    "buy_price = '" + this.buy_price + "', " +
                    "sale_price = '" + this.sale_price + "', " +
                    "tva = '"+ this.tva +"', " +
                    "updated_at = '"+ new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date()) +"'" +
                    " WHERE id = " + this.id;
        }

        MysqlQuery mysqlQuery = GlobalInstances.getCursor();
        try {
            if (mysqlQuery.query(sql_stmt)) {
                mysqlQuery.query("SELECT MAX(id) FROM products");
                if(mysqlQuery.getLastResultSet().next()) {
                    int id =  mysqlQuery.getLastResultSet().getInt(1);
                    this.setId(id);
                    return true;
                }
            }
        } catch (Exception e) {
            System.err.println("Error: " + e);
        }
        return false;
    }

    @Override
    public Product fromDatabase(ResultSet resultSet) {
        Product product = new Product();
        try {
            product.setId(resultSet.getInt(1));
            product.setLabel(resultSet.getString(2));
            product.setDescription(resultSet.getString(3));
            product.setBuy_price(resultSet.getDouble(4));
            product.setSale_price(resultSet.getDouble(5));
            product.setTva(resultSet.getDouble(6));
            product.setCreated_at(resultSet.getString(7));
            product.setUpdated_at(resultSet.getString(8));
            product.setDeleted_at(resultSet.getString(9));
        } catch (Exception e) {
            System.err.println(e);
        }
        return product;
    }

}
