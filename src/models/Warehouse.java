package models;

import controllers.GlobalInstances;
import database.MysqlQuery;

import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class Warehouse extends BaseModel {
    private int id;
    private int product_id;
    private int quantity;
    private String note;
    private String created_at;
    private String updated_at;
    private String deleted_at;

    public Warehouse() {
    }

    public Warehouse(int product_id, int quantity, String note) {
        this.id = -1;
        this.product_id = product_id;
        this.quantity = quantity;
        this.note = note;
    }

    public Warehouse(int id, int product_id, int quantity, String note) {
        this.id = id;
        this.product_id = product_id;
        this.quantity = quantity;
        this.note = note;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getProduct_id() {
        return product_id;
    }

    public void setProduct_id(int product_id) {
        this.product_id = product_id;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public String getDeleted_at() {
        return deleted_at;
    }

    public void setDeleted_at(String deleted_at) {
        this.deleted_at = deleted_at;
    }

    @Override
    public boolean save() {
        String sql_stmt;
        if(this.id == -1) {
            sql_stmt = "INSERT INTO " + getModel() + " (product_id, quantity, note) VALUES (" +
                    "'" + this.product_id + "', " +
                    "'" + this.quantity + "', " +
                    "'" + this.note + "' " +
                    ")";
        } else {
            // TODO get current timestempt
            sql_stmt = "UPDATE " + getModel() + " SET " +
                    "product_id = '" + this.product_id + "', " +
                    "quantity = '" + this.quantity + "', " +
                    "note = '" + this.note + "', " +
                    "updated_at = '"+ new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date()) +"'" +
                    " WHERE id = " + this.id;
        }

        MysqlQuery mysqlQuery = GlobalInstances.getCursor();
        try {
            if (mysqlQuery.query(sql_stmt)) {
                mysqlQuery.query("SELECT MAX(id) FROM warehouses");
                if(mysqlQuery.getLastResultSet().next()) {
                    int id = mysqlQuery.getLastResultSet().getInt(1);
                    this.setId(id);
                    return true;
                }
            }
        } catch (Exception e) {
            System.err.println("Error: " + e);
        }
        return false;
    }

    @Override
    public Warehouse fromDatabase(ResultSet resultSet) {
        Warehouse warehouse = new Warehouse();
        try {
            warehouse.setId(resultSet.getInt(1));
            warehouse.setProduct_id(resultSet.getInt(2));
            warehouse.setQuantity(resultSet.getInt(3));
            warehouse.setNote(resultSet.getString(4));
            warehouse.setCreated_at(resultSet.getString(5));
            warehouse.setUpdated_at(resultSet.getString(6));
            warehouse.setDeleted_at(resultSet.getString(7));
        } catch (Exception e) {
            System.err.println(e);
        }
        return warehouse;
    }

    public ArrayList<Warehouse> customSearch(String key, String field) {

        if(key.isEmpty()) {
            return new Warehouse().all();
        }

        ArrayList<Product> products = new Product().search(key, field);
        ArrayList<Warehouse> warehouses = new ArrayList<Warehouse>();

        for (Product product : products) {

            if(!new Warehouse().search(Integer.toString(product.getId()), "product_id").isEmpty()) {
                warehouses.addAll( (new Warehouse().search(Integer.toString(product.getId()), "product_id")) );
            }
        }

        return  warehouses;
    }
}
