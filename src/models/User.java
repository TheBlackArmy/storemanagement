package models;

import controllers.GlobalInstances;
import database.MysqlQuery;

import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.Date;

public class User extends BaseModel {
    private int id;
    private String firstname;
    private String lastname;
    private String login;
    private String phone;
    private String email;
    private String password;
    private String created_at;
    private String updated_at;
    private String deleted_at;

//    protected static String model = "users";

    public User() {
    }

    public User(String firstname, String lastname, String login, String phone, String email, String password) {
        this.id = -1;
        this.firstname = firstname;
        this.lastname = lastname;
        this.login = login;
        this.phone = phone;
        this.email = email;
        this.password = password;
    }

    public User(int id, String firstname, String lastname, String login, String phone, String email, String password) {
        this.id = id;
        this.firstname = firstname;
        this.lastname = lastname;
        this.login = login;
        this.phone = phone;
        this.email = email;
        this.password = password;
    }

    public int getId() {
        return id;
    }

    public String getFirstname() {
        return firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public String getLogin() {
        return login;
    }

    public String getPhone() {
        return phone;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    public String getCreated_at() {
        return created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public String getDeleted_at() {
        return deleted_at;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public void setDeleted_at(String deleted_at) {
        this.deleted_at = deleted_at;
    }



    @Override
    public boolean save() {
        String sql_stmt;
        if(this.id == -1) {
            sql_stmt = "INSERT INTO " + getModel() + " (firstname, lastname, login, phone, email, password) VALUES (" +
                    "'" + this.firstname + "', " +
                    "'" + this.lastname + "', " +
                    "'" + this.login + "', " +
                    "'" + this.phone + "', " +
                    "'" + this.email + "', " +
                    "'" + this.password + "'" +
                    ")";
        } else {
            // TODO get current timestempt
            sql_stmt = "UPDATE " + getModel() + " SET " +
                    "firstname = '" + this.firstname + "', " +
                    "lastname = '" + this.lastname + "', " +
                    "login = '" + this.login + "', " +
                    "phone = '" + this.phone + "', " +
                    "email = '"+ this.email +"', " +
                    "password = '" + this.password + "'," +
                    "updated_at = '"+ new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date()) +"'" +
                    " WHERE id = " + this.id;
        }

        MysqlQuery mysqlQuery = GlobalInstances.getCursor();
        try {
            if (mysqlQuery.query(sql_stmt)) {
                mysqlQuery.query("SELECT MAX(id) FROM users");
                if(mysqlQuery.getLastResultSet().next()) {
                    int id =  mysqlQuery.getLastResultSet().getInt(1);
                    this.setId(id);
                    return true;
                }
            }
        } catch (Exception e) {
            System.err.println("Error: " + e);
        }
        return false;
    }

    @Override
    public User fromDatabase(ResultSet resultSet) {
        User user = new User();
        try {
            user.setId(resultSet.getInt(1));
            user.setFirstname(resultSet.getString(2));
            user.setLastname(resultSet.getString(3));
            user.setLogin(resultSet.getString(4));
            user.setPhone(resultSet.getString(5));
            user.setEmail(resultSet.getString(6));
            user.setPassword(resultSet.getString(7));
            user.setCreated_at(resultSet.getString(8));
            user.setUpdated_at(resultSet.getString(9));
            user.setDeleted_at(resultSet.getString(10));
        } catch (Exception e) {
            System.err.println(e);
        }
        return user;
    }
}
