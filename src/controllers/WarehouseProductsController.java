package controllers;

import models.Warehouse;

import java.util.ArrayList;

public class WarehouseProductsController {

    public static ArrayList<Warehouse> getAll() {
        return new Warehouse().all();
    }

    public static Warehouse show(int id) {
        return new Warehouse().find(id);
    }

    public static Warehouse create(int product_id, int quantity, String note) {
        Warehouse warehouse = new Warehouse(product_id, quantity, note);
        if(warehouse.save()) {
            return warehouse;
        }
        return null;
    }

    public static Warehouse update(int id, int product_id, int quantity, String note) {
        Warehouse warehouse = new Warehouse(id, product_id, quantity, note);
        if(warehouse.save()) {
            return warehouse;
        }
        return null;
    }

    public static boolean destroy(int id) {
        return new Warehouse().delete(id);
    }

}
