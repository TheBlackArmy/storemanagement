package controllers;

import models.Sale;
import models.Warehouse;
import views.layouts.CustomDialog;

import java.sql.SQLException;
import java.util.ArrayList;

public class SalesController {

    public static ArrayList<Sale> getAll() {
        return new Sale().all();
    }

    public static Sale show(int id) {
        return new Sale().find(id);
    }

    public static Sale create(int product_id, int quantity) throws SQLException {

        Warehouse warehouse = new Warehouse().where("product_id", new String[] {Integer.toString(product_id)});
        if(warehouse != null) {
            if(warehouse.getQuantity() >= quantity) {
                // save sale
                Sale sale = new Sale(product_id, GlobalInstances.getConnectedUser().getId(), quantity);

                if(sale.save()) {
                    warehouse.setQuantity(warehouse.getQuantity() - quantity);
                    WarehouseProductsController.update(warehouse.getId(), warehouse.getProduct_id(), warehouse.getQuantity(), warehouse.getNote());
                    return sale;
                }
            } else {
                new CustomDialog("Out Of Stock", "Product out of stock. Please call providers to feed up the stock");
            }
        } else {
            System.out.println("Not found");
        }

        return null;
    }

    public static Sale update(int id, int product_id, int quantity) {
        Sale sale = new Sale(id, product_id, GlobalInstances.getConnectedUser().getId(), quantity);
        if(sale.save()) {
            return sale;
        }
        return null;
    }

    public static boolean destroy(int id) {
        return new Sale().delete(id);
    }

}
