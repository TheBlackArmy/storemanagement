package controllers;

import models.Provision;
import models.Warehouse;

import java.sql.SQLException;
import java.util.ArrayList;

public class ProvisionsController {

    public static ArrayList<Provision> getAll() {
        return new Provision().all();
    }

    public static Provision show(int id) {
        return new Provision().find(id);
    }

    public static Provision create(int product_id, int quantity, String note) {
        Provision provision = new Provision(product_id, quantity, note);
        if(provision.save()) {
            return provision;
        }
        return null;
    }

    public static Provision update(int id, int product_id, int quantity, String note) throws SQLException {
        Provision provision = new Provision(id, product_id, quantity, note);
        Warehouse warehouse = new Warehouse().where("product_id", new String[] {Integer.toString(product_id)});
        if(provision.save()) {
            warehouse.setQuantity(warehouse.getQuantity() + quantity);
            WarehouseProductsController.update(warehouse.getId(), warehouse.getProduct_id(), warehouse.getQuantity(), warehouse.getNote());
            return provision;
        }
        return null;
    }

    public static boolean destroy(int id) {
        return new Provision().delete(id);
    }

}
