package controllers;

import models.Product;
import models.Warehouse;

import java.util.ArrayList;

public class ProductsController {

    public static ArrayList<Product> getAll() {
        return new Product().all();
    }

    public static Product show(int id) {
        return new Product().find(id);
    }

    public static Product create(String label, String description, double buy_price, double sale_price, double tva) {
        Product product =  new Product(label, description, buy_price, sale_price, tva);

        if(product.save()) {
            WarehouseProductsController.create(product.getId(), 0, product.getDescription());
            return product;
        }
        return null;
    }

    public static Product update(int id, String label, String description, double buy_price, double sale_price, double tva) {
        Product product =  new Product(id, label, description, buy_price, sale_price, tva);
        if(product.save()) {
            return product;
        }
        return null;
    }

    public static boolean destroy(int id) {
        return new Product().delete(id);
    }

}
