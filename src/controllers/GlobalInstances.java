package controllers;

import database.MysqlQuery;
import models.User;

public class GlobalInstances {
    private static User connectedUser;
    private static MysqlQuery cursor = null;

    public GlobalInstances() {
    }

    public static User getConnectedUser() {
        return connectedUser;
    }

    public static void setConnectedUser(User connectedUser) {
        GlobalInstances.connectedUser = connectedUser;
    }

    public static MysqlQuery getCursor() {
        if(cursor == null) {
            cursor =  new MysqlQuery("localhost", "admin", "admin", "store_mgmt");
        }
        return cursor;
    }
}
