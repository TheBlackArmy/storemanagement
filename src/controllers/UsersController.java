package controllers;

import models.User;

import java.util.ArrayList;

public class UsersController {

    public static ArrayList<User> getAll() {
        return new User().all();
    }

    public static User show(int id) {
        return new User().find(id);
    }

    public static User create(String firstname, String lastname, String login, String phone, String email, String password) {
        User user = new User(firstname, lastname, login, phone, email, password);
        if(user.save()) {
            return user;
        }
        return null;

    }

    public static User update(int id, String firstname, String lastname, String login, String phone, String email, String password) {
        User user = new User(id, firstname, lastname, login, phone, email, password);
        if(user.save()) {
            return user;
        }
        return null;
    }

    public static boolean destroy(int id) {
        return new User().delete(id);
    }

}
