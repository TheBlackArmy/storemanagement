-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Apr 09, 2020 at 06:31 PM
-- Server version: 10.3.20-MariaDB-0ubuntu0.19.04.1
-- PHP Version: 7.2.24-0ubuntu0.19.04.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `store_mgmt`
--

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `label` varchar(100) NOT NULL,
  `description` tinytext NOT NULL,
  `buy_price` double NOT NULL,
  `sale_price` double NOT NULL,
  `tva` double NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `label`, `description`, `buy_price`, `sale_price`, `tva`, `created_at`, `updated_at`, `deleted_at`) VALUES
(2, 'Flocon d\'avoine', 'Pas de description', 800, 990, 0.18, '2020-04-04 00:00:00', '2020-10-10 00:00:00', NULL),
(3, 'Oeuf', 'Pas de description', 1390, 1990, 0.18, '2020-04-08 14:44:48', '2020-10-10 00:00:00', NULL),
(5, 'Cereales', 'Pas de description', 2000, 2590, 0.18, '2020-04-09 15:39:45', '2020-04-09 05:03:16', NULL),
(6, 'Test', 'Test', 23, 345, 0.18, '2020-04-09 15:42:04', NULL, NULL),
(7, 'Test 67', 'Test', 23, 345, 0.18, '2020-04-09 15:42:34', '2020-04-09 04:57:00', NULL),
(8, 'Abrico', 'Pas de description', 3400, 4000, 0.18, '2020-04-09 15:45:09', NULL, NULL),
(10, 'drtgfgf', '', 233, 43, 0.18, '2020-04-09 16:43:42', NULL, NULL),
(11, 'Test 2', '', 34, 54, 0.18, '2020-04-09 16:54:37', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `provisions`
--

CREATE TABLE `provisions` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `note` tinytext NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `provisions`
--

INSERT INTO `provisions` (`id`, `product_id`, `quantity`, `note`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 2, 24, 'On etait en manque', '2020-04-08 00:00:00', NULL, NULL),
(2, 2, 12, 'pas de note', '2020-04-08 17:09:00', '2020-10-10 00:00:00', NULL),
(3, 2, 5, 'Pas de note', '2020-04-09 15:20:43', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sales`
--

CREATE TABLE `sales` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `sales`
--

INSERT INTO `sales` (`id`, `product_id`, `user_id`, `quantity`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 2, 10, 5, '2020-04-08 00:00:00', NULL, NULL),
(2, 2, 12, 0, '2020-04-08 16:34:03', NULL, NULL),
(3, 5, 12, 12, '2020-04-09 13:13:34', '2020-04-09 05:07:37', NULL),
(6, 2, 12, 5, '2020-04-09 15:11:08', NULL, NULL),
(7, 2, 12, 5, '2020-04-09 15:15:30', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `firstname` varchar(20) NOT NULL,
  `lastname` varchar(20) NOT NULL,
  `login` varchar(20) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `email` varchar(30) NOT NULL,
  `password` varchar(60) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `firstname`, `lastname`, `login`, `phone`, `email`, `password`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'John', 'Doe', 'John Doe', 'null', 'john@email.com', 'johndoe', '2020-04-04 00:00:00', '2020-10-10 00:00:00', NULL),
(3, 'Christian', 'Nilson', 'chris', '785473763', 'chris@email.com', 'chris', '2020-04-07 14:16:43', '2020-04-09 05:04:26', NULL),
(10, 'Leroy Mataa', 'Abiguime', 'leroy', '783475466', 'leroy@email.com', '', '2020-04-07 16:36:55', '2020-04-09 03:48:49', NULL),
(11, 'Anita', 'Lisborn', 'anita', '894384535', 'anita@email.com', 'anita', '2020-04-07 16:41:42', NULL, NULL),
(12, 'Annie', 'Wilson', 'annie', '88774885', 'annie@email.com', 'annie', '2020-04-07 16:45:09', NULL, NULL),
(13, 'Test 7', 'Test', 'Test', 'Test', 'Test', 'dsf', '2020-04-09 17:05:05', '2020-04-09 05:08:46', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `warehouses`
--

CREATE TABLE `warehouses` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `note` tinytext DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `warehouses`
--

INSERT INTO `warehouses` (`id`, `product_id`, `quantity`, `note`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 2, 30, 'On en a assez', '2020-04-08 00:00:00', '2020-04-09 03:15:31', NULL),
(4, 8, 0, 'Pas de description', '2020-04-09 15:45:10', NULL, NULL),
(6, 10, 0, '', '2020-04-09 16:43:42', NULL, NULL),
(7, 11, 0, '', '2020-04-09 16:54:38', NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `label` (`label`);

--
-- Indexes for table `provisions`
--
ALTER TABLE `provisions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_id_2` (`product_id`);

--
-- Indexes for table `sales`
--
ALTER TABLE `sales`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_id` (`product_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `login` (`login`,`phone`,`email`,`password`);

--
-- Indexes for table `warehouses`
--
ALTER TABLE `warehouses`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `product_id` (`product_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `provisions`
--
ALTER TABLE `provisions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `sales`
--
ALTER TABLE `sales`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `warehouses`
--
ALTER TABLE `warehouses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `provisions`
--
ALTER TABLE `provisions`
  ADD CONSTRAINT `provisions_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `sales`
--
ALTER TABLE `sales`
  ADD CONSTRAINT `sales_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `sales_ibfk_2` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `warehouses`
--
ALTER TABLE `warehouses`
  ADD CONSTRAINT `warehouses_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
